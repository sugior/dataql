---
id: v4.2.1
title: v4.2.1 (2020-10-16)
---

# v4.2.1 (2020-10-16)

## 优化
- 未能识别的数据库可以通过 `HASOR_DATAQL_DATAWAY_DB_DBTYPE` 环境变量强制指定
- 删除配置项 `HASOR_DATAQL_FX_PAGE_DIALECT` 。方言首先优先从 hint 中获取，其次会根据数据源的链接串进行自动推断。如果都找不到才会爆异常。

## 修复
- 修复 `FRAGMENT_SQL_COLUMN_CASE` 失效的 Bug。